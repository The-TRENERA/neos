0.10 (maybe 1.2.0?):
- Updated to EOS SDK 1.2.0
Most new features from it are currently just stubs though, but small parts are implemented.

- Added Release-1.1.0 build config.
In this config the _120.cpp files are excluded from the build, and the EOS_BUILD_110 define is set.
This should allow us to build a 1.1.0 compatible DLL, so we can keep supporting all the existing EOS games!
The EOS_BUILD_110 define allows us to swap out the function prototype for functions that changed between 1.1.0 and 1.2.0 (eg. EOS_Auth_AddNotifyLoginStatusChanged which had a parameter added to it)
While this solution should work out fine, it'll mean needing two sets of Win64/32 DLLs for each nEOS release, one for 1.2.0 and the other 1.1.0...

- Fixed Win32 DLL: now exports are named like the proper EOS DLL.
Previously I used a module .def file to store all the export names/ordinals & their order, so if I was missing the code for an export VS would let me know about it.
Unfortunately using this .def messed up the names of the Win32 builds exports, so most 32-bit games wouldn't work.
(I pushed out a 0.9a release to fix this, but that fix was backported from this version)

- Implemented most of 1.2.0's HConnect functions, and a few new HEcom functions (eg EOS_Ecom_QueryEntitlements)
Haven't been able to test any of this implementation tho as no games currently use it...
(maybe worth trying it against the samples included with EOS SDK?)

- Added proxy code to all the stubbed 1.2.0 functions - proxying 1.2.0 should hopefully work fine!

- Allow overriding HPlatform's new EncryptionKey & DeploymentId values.
DeploymentId seems to be a new kind of AppID sorta, while EncryptionKey is supposedly for EOS to encrypt files related to the game.
No idea what kind of files are encrypted with the key though, but at least now you can override it :)

- Print extended 1.2.0 EOS_Platform_Options values if game uses them
(EncryptionKey / OverrideCountryCode / OverrideLocaleCode / DeploymentId / Flags...)

- Warn user if game tries using any newer ApiVersion than nEOS was built for.

- Code: improved proxying macros, so less code is needed to start proxying a function.
(Removes any need for seperate eos-proxy headers too!)

- Code: now store original HEcom/HPlatform handles from EOS when in proxy mode.

TODO:
- Ensure compatibility with 1.1.0 SDK (and all the games that worked previously with nEOS)
Maybe even older SDKs too? Really hope I can get hold of one...

0.9:
- Fixed async Ecom_QueryOwnership & made all other async functions fully async - should be closer to actual EOS now.

- Added HPlatform & HAuth classes to hold the platform/auth state, instead of using globals.

- Added thread-safety mutexes to logging functions & login-status functions.

0.8a:
- Added ForcedDLC->UseMalloc option (default 0), to try using malloc to allocate DLC IDs if the game didn't provide an allocation function.
Seems to work with The Sinking City, maybe can help with other games that don't provide alloc functions too.

- Now uses nlohmann::fifo_map instead of std::map, to preserve insertion order of DLCs during EOS_Ecom_QueryOwnership & INI read.

- Prepping for source release...

0.8:
- Fixed async mode by changing how callbacks work: now calculates/checks stuff synchonously and only sends the result asynchronously.
(probably not how it's meant to be done - really everything should be done async so it doesn't stall the game - but because we don't have code for storing inputs etc it's the easiest way)
This should make the UseAsyncCallbacks option no longer needed hopefully, but I've left it there in case of any problems, will probably be removed in a later release if all goes well though!
Also changed callback vector to std::deque instead, since that seems a lot faster to iterate over, and changed the code for iterating to allow new callbacks to be added while we're running them.

- Changed UseAsyncCallbacks to default to 1, hopefully disabling them shouldn't be necessary anymore! I'll probably remove this in a later version if everything works out fine.

- Fixed faulty EOS_Ecom_QueryOwnership: now uses AllocateMemoryFunction provided by the game to alloc memory for DLC IDs (though if game doesn't provide one - eg The Sinking City - DLC forcing won't be able to work, maybe I'll find a fix for 0.9..)

- Added DLC->LogQueries INI option, so you can silence DLC query output in the log file (some games spam this, could slow game down writing it all to log file...)

- Will now copy over any existing command-line when relaunching (so eg. running WRC8.exe -epiclocale=es-ES should run the game in spanish hopefully, though you could also edit nEOS.ini for it too)

- Finally passed the 100KiB mark :8=)

0.7:
- Added "UseAsyncCallbacks" option to INI (default 0), this can let you disable the async-related code in nEOS (as some games like The Sinking City don't seem to work with it)
Probably best to leave this at 0, I don't know any game that requires it to be 1, I'll probably just remove all the async stuff in future if nothing actually needs it.

- nEOS will now detect if the game is being ran without command-line args that some games require, and relaunch with a fixed command-line if needed
This relaunch should be pretty transparent, it's the same method CODEX use in their emu I believe.
Make sure to set the correct ProductId in the INI file if the game requires it!

- Added "Override" section to INI, when in proxy mode you can use this to override the product info the game sends over to EGS
Eg. you could pretend to be playing Fortnite when you're actually playing some game you don't own.
This should let you get access to any EOS "features" your game might make use of.
(kinda like a Steamworks fix in a way, but EOS doesn't really have any networking stuff afaik so not really as useful)

- Should now always log/read INI from the same folder as the nEOS DLL

0.6 (will identify as 0.5 in log file because I'm a dummy):
- Added some thread-safety mutexes to the callback functions, should help prevent any issues if a game uses EOS functions in multiple threads

- Removed login display code for now: fixes crashing for some people
(previously it would try displaying some login info provided by the game in the log file - don't worry, no passwords or anything, just token/tickets and stuff like that - seems for some people there's some extra info available which I don't handle properly atm, so I'm gonna comment out the code for this till I can get around to fixing it up)

- Added a lot more sanity checks to some functions (making sure game has called EOS_Initialize beforehand etc), just to work closer to original EOS DLL.

- Some code cleanup, better organization etc.

0.5:
- Added "proxy mode", similar to CreamAPI where functions can be sent to the original EOSSDK DLL (except DLC functions, which we still control with the INI file) - haven't been able to test this however, but hopefully it should work out fine!

0.4:
- Added support for "on-login-status-changed" callbacks, should help support any games that require them
- Added log-levels & filtering log by level
- Log some more things
- Code cleanup & now makes use of precompiled header

0.31:
- Added configurable user info (name/ID/language/country) to nEOS.ini - Control doesn't seem to use this, but other games might

0.3a:
- Hotfix for ForcedDLC, hopefully shouldn't cause any crashes now.

0.3:
- Added nEOS.ini with configurable DLC / logging options