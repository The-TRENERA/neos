#include "../stdafx.h"
#include "../NEOS.hpp"

EOS_Ecom_Entitlement* HEcom::CacheEntitlement(size_t instanceId)
{
	auto* exists = GetEntitlementByInstanceId(instanceId);
	if (exists)
		return exists;

	if (instanceId >= Entitlements.size())
		return nullptr;

	std::lock_guard<std::mutex> guard(m_EntitlementsMutex);

	auto* entitlement = new EOS_Ecom_Entitlement();
	entitlement->ApiVersion = EOS_ECOM_ENTITLEMENT_API_LATEST;
	entitlement->bRedeemed = false;

	auto& pair = Entitlements.at(instanceId);
	auto& entitlementId = pair.first;
	auto& catalogItemId = pair.second;

	auto* entitlementIdRaw = new char[entitlementId.length() + 1];
	memcpy(entitlementIdRaw, entitlementId.c_str(), entitlementId.length());
	entitlementIdRaw[entitlementId.length()] = 0;
	entitlement->Id = entitlementIdRaw;

	auto* catalogItemIdRaw = new char[catalogItemId.length() + 1];
	memcpy(catalogItemIdRaw, catalogItemId.c_str(), catalogItemId.length());
	catalogItemIdRaw[catalogItemId.length()] = 0;
	entitlement->CatalogItemId = catalogItemIdRaw;

	auto instanceIdStr = std::to_string(instanceId);

	auto* instanceIdRaw = new char[instanceIdStr.length() + 1];
	memcpy(instanceIdRaw, instanceIdStr.c_str(), instanceIdStr.length());
	instanceIdRaw[instanceIdStr.length()] = 0;
	entitlement->InstanceId = entitlementIdRaw;

	entitlement->ServerIndex = -1;

	m_Entitlements.push_back(entitlement);

	return entitlement;
}


EOS_Ecom_Entitlement* HEcom::GetEntitlementByEntitlementId(const std::string& entitlementId)
{
	std::lock_guard<std::mutex> guard(m_EntitlementsMutex);
	for (auto ent : m_Entitlements)
	{
		if (entitlementId == ent->Id && !ent->bRedeemed) // TODO: should we be checking redeemed?
			return ent;
	}

	return nullptr;
}

EOS_Ecom_Entitlement* HEcom::GetEntitlementByInstanceId(size_t instanceId)
{
	std::lock_guard<std::mutex> guard(m_EntitlementsMutex);
	for (auto ent : m_Entitlements)
	{
		size_t entInstanceId = std::strtoull(ent->InstanceId, 0, 0);
		if (entInstanceId == instanceId)
			return ent;
	}

	return nullptr;
}

EOS_Ecom_Entitlement* HEcom::GetEntitlementByIndex(size_t index)
{
	std::lock_guard<std::mutex> guard(m_EntitlementsMutex);
	if (index >= m_Entitlements.size())
		return nullptr;
	
	return m_Entitlements.at(index);
}