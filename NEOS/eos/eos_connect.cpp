#include "../stdafx.h"
#include "../NEOS.hpp"

/**
 * The Connect Interface is used to manage local user permissions and access to backend services through the verification of various forms of credentials.
 * It creates an association between third party providers and an internal mapping that allows Epic Online Services to represent a user agnostically
 * All Connect Interface calls take a handle of type EOS_HConnect as the first parameter.
 * This handle can be retrieved from a EOS_HPlatform handle by using the EOS_Platform_GetConnectInterface function.
 *
 * NOTE: At this time, this feature is only available for products that are part of the Epic Games store.
 *
 * @see EOS_Platform_GetConnectInterface
 */

/**
 * Login/Authenticate given a valid set of external auth credentials.
 *
 * @param Options structure containing the external account credentials and type to use during the login operation
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the login operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Connect_Login(EOS_HConnect Handle, const EOS_Connect_LoginOptions* Options, void* ClientData, const EOS_Connect_OnLoginCallback CompletionDelegate)
{
	log(LL::Info, "EOS_Connect_Login (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_CONNECT_LOGIN_API_LATEST);

	// TODO: code below seems to cause crashes for some ppl, not sure what makes it happen though...
	/*if (Options && Options->Credentials)
	{
		if (Options->Credentials->Id && strlen(Options->Credentials->Id))
			log(LL::Info, " - Login Id: %s", Options->Credentials->Id);
		if (Options->Credentials->Token && strlen(Options->Credentials->Token))
			log(LL::Info, " - Login Token: %s", Options->Credentials->Token);
		log(LL::Info, " - Login Type: %d", Options->Credentials->Type);
	}*/

	PROXY_FUNC(EOS_Connect_Login);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	//currently unused:
	//EOS_Connect_LoginOptions options;
	//if (Options)
	//	options = *Options;

	NEOS_AddCallback([Handle, Options, ClientData, CompletionDelegate]()
		{
			auto* auth = reinterpret_cast<HAuth*>(Handle);

			if (auth->GetLoginStatus() == EOS_ELoginStatus::EOS_LS_LoggedIn)
				log(LL::Warning, __FUNCTION__ ": already logged in!");

			auth->ChangeLoginStatus(EOS_ELoginStatus::EOS_LS_LoggedIn);

			EOS_Connect_LoginCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = UserProductId;
			cbi.ContinuanceToken = nullptr;
			CompletionDelegate(&cbi);
		});
}

/**
 * Create an account association with the Epic Online Service as a product user given their external auth credentials
 *
 * @param Options structure containing a continuance token from a "user not found" response during Login (always try login first)
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the create operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Connect_CreateUser(EOS_HConnect Handle, const EOS_Connect_CreateUserOptions* Options, void* ClientData, const EOS_Connect_OnCreateUserCallback CompletionDelegate)
{
	log(LL::Info, "EOS_Connect_CreateUser (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_CONNECT_CREATEUSER_API_LATEST);

	PROXY_FUNC(EOS_Connect_CreateUser);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	//currently unused:
	//EOS_Connect_LoginOptions options;
	//if (Options)
	//	options = *Options;

	NEOS_AddCallback([Handle, Options, ClientData, CompletionDelegate]()
		{
			auto* auth = reinterpret_cast<HAuth*>(Handle);

			if (auth->GetLoginStatus() == EOS_ELoginStatus::EOS_LS_LoggedIn)
				log(LL::Warning, __FUNCTION__ ": already logged in!");

			auth->ChangeLoginStatus(EOS_ELoginStatus::EOS_LS_LoggedIn);

			EOS_Connect_CreateUserCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = UserProductId;
			CompletionDelegate(&cbi);
		});
}

/**
 * Link a set of external auth credentials with an existing product user on the Epic Online Service
 *
 * @param Options structure containing a continuance token from a "user not found" response during Login (always try login first) and a currently logged in user not already associated with this external auth provider
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the link operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Connect_LinkAccount(EOS_HConnect Handle, const EOS_Connect_LinkAccountOptions* Options, void* ClientData, const EOS_Connect_OnLinkAccountCallback CompletionDelegate)
{
	log(LL::Debug, "EOS_Connect_LinkAccount (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_CONNECT_LINKACCOUNT_API_LATEST);

	PROXY_FUNC(EOS_Connect_LinkAccount);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	//currently unused:
	//EOS_Connect_LoginOptions options;
	//if (Options)
	//	options = *Options;

	NEOS_AddCallback([Handle, Options, ClientData, CompletionDelegate]()
		{
			auto* auth = reinterpret_cast<HAuth*>(Handle);

			if (auth->GetLoginStatus() == EOS_ELoginStatus::EOS_LS_LoggedIn)
				log(LL::Warning, __FUNCTION__ ": already logged in!");

			auth->ChangeLoginStatus(EOS_ELoginStatus::EOS_LS_LoggedIn);

			EOS_Connect_LinkAccountCallbackInfo cbi;
			cbi.ResultCode = Options ? EOS_EResult::EOS_Success : EOS_EResult::EOS_InvalidParameters;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = UserProductId;
			CompletionDelegate(&cbi);
		});
}

/**
 * Retrieve the equivalent account id mappings for a list of external account ids from supported account providers.  The values will be cached and retrievable via EOS_Connect_QueryExternalAccountMappings
 *
 * @param Options structure containing a list of external account ids, in string form, to query for the Epic service account representation
 * @param ClientData arbitrary data that is passed back to you in the CompletionDelegate
 * @param CompletionDelegate a callback that is fired when the query operation completes, either successfully or in error
 */
EOS_DECLARE_FUNC(void) EOS_Connect_QueryExternalAccountMappings(EOS_HConnect Handle, const EOS_Connect_QueryExternalAccountMappingsOptions* Options, void* ClientData, const EOS_Connect_OnQueryExternalAccountMappingsCallback CompletionDelegate)
{
	if(Options)
		log(LL::Debug, "EOS_Connect_QueryExternalAccountMappings (%p, %p(%d, %p, %d, %p, %d), %p, %p)", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->AccountIdType, Options->ExternalAccountIds, Options->ExternalAccountIdCount, ClientData, CompletionDelegate);
	else
		log(LL::Debug, "EOS_Connect_QueryExternalAccountMappings (%p, %p, %p, %p)", Handle, Options, ClientData, CompletionDelegate);

	EOS_CHECK_VERSION(EOS_CONNECT_QUERYEXTERNALACCOUNTMAPPINGS_API_LATEST);

	PROXY_FUNC(EOS_Connect_QueryExternalAccountMappings);
	if (proxied)
	{
		proxied(Handle, Options, ClientData, CompletionDelegate);
		return;
	}

	//TODOASYNC
}

/**
 * Fetch a product user id that maps to an external account id
 *
 * @param Options structure containing the local user and target external account id
 *
 * @return the product user id for a target external account previously retrieved from the backend service
 */
EOS_DECLARE_FUNC(EOS_ProductUserId) EOS_Connect_GetExternalAccountMapping(EOS_HConnect Handle, const EOS_Connect_GetExternalAccountMappingsOptions* Options)
{
	if(Options && Options->TargetExternalUserId)
		log(LL::Debug, "EOS_Connect_GetExternalAccountMapping (%p, %p(%d, %p, %d, %p(%s)))", Handle, Options, Options->ApiVersion, Options->LocalUserId, Options->AccountIdType, Options->TargetExternalUserId, Options->TargetExternalUserId);
	else
		log(LL::Debug, "EOS_Connect_GetExternalAccountMapping (%p, %p)", Handle, Options);

	EOS_CHECK_VERSION(EOS_CONNECT_GETEXTERNALACCOUNTMAPPINGS_API_LATEST);

	PROXY_FUNC(EOS_Connect_GetExternalAccountMapping);
	if (proxied)
	{
		auto res = proxied(Handle, Options);
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	if (!Options)
		return nullptr;

	// TODOSTUB
	return nullptr;
}

/**
 * Fetch the number of product users that are logged in.
 *
 * @return the number of product users logged in.
 */
EOS_DECLARE_FUNC(int32_t) EOS_Connect_GetLoggedInUsersCount(EOS_HConnect Handle)
{
	log(LL::Debug, "EOS_Connect_GetLoggedInUsersCount (%p)", Handle);

	PROXY_FUNC(EOS_Connect_GetLoggedInUsersCount);
	if (proxied)
	{
		auto res = proxied(Handle);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	return 1;
}

/**
 * Fetch a product user id that is logged in. This product user id is in the Epic Online Services namespace
 *
 * @param Index an index into the list of logged in users. If the index is out of bounds, the returned product user id will be invalid.
 *
 * @return the product user id associated with the index passed
 */
EOS_DECLARE_FUNC(EOS_ProductUserId) EOS_Connect_GetLoggedInUserByIndex(EOS_HConnect Handle, int32_t Index)
{
	log(LL::Debug, "EOS_Connect_GetLoggedInUserByIndex (%p, %d)", Handle, Index);

	PROXY_FUNC(EOS_Connect_GetLoggedInUserByIndex);
	if (proxied)
	{
		auto res = proxied(Handle, Index);
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	if (Index != 0)
		return nullptr;

	return UserProductId;
}

/**
 * Fetches the login status for an product user id.  This product user id is considered logged in as long as the underlying access token has not expired.
 *
 * @param LocalUserId the product user id of the user being queried
 *
 * @return the enum value of a user's login status
 */
EOS_DECLARE_FUNC(EOS_ELoginStatus) EOS_Connect_GetLoginStatus(EOS_HConnect Handle, EOS_ProductUserId LocalUserId)
{
	log(LL::Debug, "EOS_Connect_GetLoginStatus (%p, %p)", Handle, LocalUserId);

	PROXY_FUNC(EOS_Connect_GetLoginStatus);
	if (proxied)
	{
		auto res = proxied(Handle, LocalUserId);
		log(LL::Debug, " + Proxy Result: %s (%d)", EnumToString(EOSEnum::ELoginStatus, (int32_t)res), res);
		return res;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_ELoginStatus::EOS_LS_NotLoggedIn;
	}

	auto* auth = reinterpret_cast<HAuth*>(Handle);

	if (LocalUserId == UserProductId)
		return auth->GetLoginStatus();

	return EOS_ELoginStatus::EOS_LS_NotLoggedIn;
}

/**
 * Register to receive upcoming authentication expiration notifications.
 * Notification is approximately 10 minutes prior to expiration.
 * Call EOS_Connect_Login again with valid third party credentials to refresh access
 *
 * @note must call RemoveNotifyAuthExpiration to remove the notification
 *
 * @param Options structure containing the API version of the callback to use
 * @param ClientData arbitrary data that is passed back to you in the callback
 * @param Notification a callback that is fired when the authentication is about to expire
 *
 * @return handle representing the registered callback
 */
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_Connect_AddNotifyAuthExpiration(EOS_HConnect Handle, const EOS_Connect_AddNotifyAuthExpirationOptions* Options, void* ClientData, const EOS_Connect_OnAuthExpirationCallback Notification)
{
	if (Options)
		log(LL::Debug, "EOS_Connect_AddNotifyAuthExpiration (%p(%d), %p, %p)", Options, Options->ApiVersion, ClientData, Notification);
	else
		log(LL::Debug, "EOS_Connect_AddNotifyAuthExpiration (%p, %p, %p)", Options, ClientData, Notification);

	EOS_CHECK_VERSION(EOS_CONNECT_ADDNOTIFYAUTHEXPIRATION_API_LATEST);

	PROXY_FUNC(EOS_Connect_AddNotifyAuthExpiration);
	if (proxied)
	{
		auto res = proxied(Handle, Options, ClientData, Notification);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	// TODOSTUB
	return EOS_INVALID_NOTIFICATIONID;
}

/**
 * Unregister from receiving expiration notifications.
 *
 * @param InId handle representing the registered callback
 */
EOS_DECLARE_FUNC(void) EOS_Connect_RemoveNotifyAuthExpiration(EOS_HConnect Handle, EOS_NotificationId InId)
{
	log(LL::Debug, "EOS_Connect_RemoveNotifyAuthExpiration (%d)", InId);

	PROXY_FUNC(EOS_Connect_RemoveNotifyAuthExpiration);
	if (proxied)
	{
		proxied(Handle, InId);
		return;
	}

	// TODOSTUB
	return;
}

/**
 * Register to receive user login status updates.
 * @note must call RemoveNotifyLoginStatusChanged to remove the notification
 *
 * @param Options structure containing the API version of the callback to use
 * @param ClientData arbitrary data that is passed back to you in the callback
 * @param Notification a callback that is fired when the login status for a user changes
 *
 * @return handle representing the registered callback
 */
EOS_DECLARE_FUNC(EOS_NotificationId) EOS_Connect_AddNotifyLoginStatusChanged(EOS_HConnect Handle, const EOS_Connect_AddNotifyLoginStatusChangedOptions* Options, void* ClientData, const EOS_Connect_OnLoginStatusChangedCallback Notification)
{
	// TODO120: Options
	log(LL::Debug, "EOS_Connect_AddNotifyLoginStatusChanged (%p, %p, %p)", Handle, ClientData, Notification);

	EOS_CHECK_VERSION(EOS_CONNECT_ADDNOTIFYLOGINSTATUSCHANGED_API_LATEST);

	PROXY_FUNC(EOS_Connect_AddNotifyLoginStatusChanged);
	if (proxied)
	{
		auto res = proxied(Handle, Options, ClientData, Notification);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return EOS_INVALID_NOTIFICATIONID;
	}

	auto* auth = reinterpret_cast<HAuth*>(Handle);

	// Note: callback body must remain part of the callback!
	auto idx = auth->LoginCallbackAdd([auth, ClientData, Notification]()
		{
			EOS_Connect_LoginStatusChangedCallbackInfo cbi;
			cbi.ClientData = ClientData;
			cbi.LocalUserId = UserProductId;
			cbi.PreviousStatus = auth->GetPrevLoginStatus();
			cbi.CurrentStatus = auth->GetLoginStatus();
			Notification(&cbi);
		});

	return NEOS_NOTIFY2_FIRSTID + idx;
}

/**
 * Unregister from receiving user login status updates.
 *
 * @param InId handle representing the registered callback
 */
EOS_DECLARE_FUNC(void) EOS_Connect_RemoveNotifyLoginStatusChanged(EOS_HConnect Handle, EOS_NotificationId InId)
{
	log(LL::Debug, "EOS_Connect_RemoveNotifyLoginStatusChanged (%p, %lld)", Handle, InId);

	PROXY_FUNC(EOS_Connect_RemoveNotifyLoginStatusChanged);
	if (proxied)
	{
		proxied(Handle, InId);
		return;
	}

	if (!Handle)
	{
		log(LL::Error, __FUNCTION__ ": invalid Handle parameter!");
		return;
	}

	auto* auth = reinterpret_cast<HAuth*>(Handle);

	if (InId < NEOS_NOTIFY2_FIRSTID)
	{
		log(LL::Warning, __FUNCTION__ ": invalid InId %p!", InId);
		return;
	}
	size_t index = InId - NEOS_NOTIFY2_FIRSTID;
	auth->LoginCallbackRemove(index);
}
