#include "../stdafx.h"
#include "../NEOS.hpp"

/** 
 * Check whether or not the given account unique id is considered valid
 * 
 * @param AccountId The account id to check for validity
 * @return EOS_TRUE if the EOS_AccountId is valid, otherwise EOS_FALSE
 */
#ifdef EOS_BUILD_120
EOS_DECLARE_FUNC(EOS_Bool) EOS_EpicAccountId_IsValid(EOS_EpicAccountId AccountId)
#else
EOS_DECLARE_FUNC(EOS_Bool) EOS_AccountId_IsValid(EOS_EpicAccountId AccountId)
#endif
{
	log(LL::Debug, "EOS_EpicAccountId_IsValid (%p)", AccountId);

	PROXY_FUNC(EOS_EpicAccountId_IsValid);
	if (proxied)
	{
		auto res = proxied(AccountId);
		log(LL::Debug, " + Proxy Result: %d", res);
		return res;
	}
	
	return AccountId == UserAccountId;
}

/**
 * Retrieve an EOS_AccountId from a raw account ID string. The input string must be null-terminated.
 *
 * @param AccountIdString The string-ified account ID for which to retrieve the EOS_AccountId
 * @return The EOS_AccountId that corresponds to the AccountIdString
 */
#ifdef EOS_BUILD_120
EOS_DECLARE_FUNC(EOS_EpicAccountId) EOS_EpicAccountId_FromString(const char* AccountIdString)
#else
EOS_DECLARE_FUNC(EOS_EpicAccountId) EOS_AccountId_FromString(const char* AccountIdString)
#endif
{
	log(LL::Debug, "EOS_EpicAccountId_FromString (%p(%s))", AccountIdString, AccountIdString);

	PROXY_FUNC(EOS_EpicAccountId_FromString);
	if (proxied)
	{
		auto res = proxied(AccountIdString);
		log(LL::Debug, " + Proxy Result: %p", res);
		return res;
	}

	return reinterpret_cast<EOS_EpicAccountId>(std::strtoull(AccountIdString, 0, 0));
}

/**
 * Retrieve a string-ified account ID from an EOS_AccountId. This is useful for replication of account IDs in multiplayer games.
 *
 * @param AccountId The account ID for which to retrieve the string-ified version.
 * @param OutBuffer The buffer into which the character data should be written
 * @param InOutBufferLength The size of the OutBuffer in characters. 
 *                          The input buffer should include enough space to be null-terminated.
 *                          When the function returns, this parameter will be filled with the length of the string copied into OutBuffer.
 *
 * @return An EOS_EResult that indicates whether the account ID string was copied into the OutBuffer.
 *         EOS_Success - The OutBuffer was filled, and InOutBufferLength contains the number of characters copied into OutBuffer excluding the null terminator.
 *         EOS_InvalidParameters - Either OutBuffer or InOutBufferLength were passed as NULL parameters.
 *         EOS_InvalidUser - The AccountId is invalid and cannot be string-ified
 *         EOS_LimitExceeded - The OutBuffer is not large enough to receive the account ID string. InOutBufferLength contains the required minimum length to perform the operation successfully.
 */
#ifdef EOS_BUILD_120
EOS_DECLARE_FUNC(EOS_EResult) EOS_EpicAccountId_ToString(EOS_EpicAccountId AccountId, char* OutBuffer, int32_t* InOutBufferLength)
#else
EOS_DECLARE_FUNC(EOS_EResult) EOS_AccountId_ToString(EOS_EpicAccountId AccountId, char* OutBuffer, int32_t* InOutBufferLength)
#endif
{
	log(LL::Debug, "EOS_EpicAccountId_ToString (%p, %p, %p(%d))", AccountId, OutBuffer, InOutBufferLength, *InOutBufferLength);

	PROXY_FUNC(EOS_EpicAccountId_ToString);
	if (proxied)
	{
		auto res = proxied(AccountId, OutBuffer, InOutBufferLength);
		log(LL::Debug, " + Proxy Result: %s (%d)", EResultToString(res), res);
		return res;
	}

	EOS_CHECK_CONFIGURED();

	if (!OutBuffer || !InOutBufferLength)
		return EOS_EResult::EOS_InvalidParameters;

	size_t bufferLength = static_cast<size_t>(*InOutBufferLength);

	auto id_str = ToString(reinterpret_cast<uint64_t>(AccountId));

	if (id_str.length() > bufferLength)
		return EOS_EResult::EOS_LimitExceeded;

	memset(OutBuffer, 0, bufferLength);

	auto copyLen = std::min(bufferLength, id_str.length());
	memcpy(OutBuffer, id_str.c_str(), copyLen);
	*InOutBufferLength = static_cast<int32_t>(copyLen);

	return EOS_EResult::EOS_Success;
}
