nEOS 0.8a - by infogram
--
nEOS is a lightwight Epic Online Services emulator DLL, which does the absolute minimum required to allow a game to run, with some very promising results!

Installing is simple, just backup any original EOSSDK DLL and replace it with the one from this zip.

With that in place, hopefully the game should now run without problems!

Unlocked DLC can be specified inside the nEOS.ini file: you can either set DLC->AllOwned to 1, or manually specify the DLC ID there.
DLC IDs can normally be found by checking the log file - any DLCs the game checks for will be written there.
(there's also an experimental DLC "forcing" section too, only try that as a last resort!)

nEOS was initially created for (and works great) with Control v1.04 - thanks to everyone on the Control thread that helped test!
Most other EOS-enabled games should work fine with it too, but if you have any problems please let me know!
(also if you have any games that need EOS and can test this with them, it'd be really appreciated!)

If your game fails to run after installing this DLL, make sure you have VC2019 redist installed: https://aka.ms/vs/16/release/vc_redist.x86.exe & https://aka.ms/vs/16/release/vc_redist.x64.exe

-- Proxy Mode --
0.5 adds a proxy mode to nEOS, similar to how CreamAPI works: all the EOS functions will be sent over to the original EOS DLL if it exists.
Except for the DLC functions, which will still work as above (using the entries in the nEOS.ini file to unlock DLC items, etc)
To use this mode just rename the original EOSSDK file to "EOSSDK-Win64-Shipping_o.dll", or change the ProxyFilename to the filename of the original DLL.

This mode hasn't been well tested yet however, but hopefully it should work fine.

-- License --
Makes use of fifo_map by nlohmann, licensed under MIT (https://github.com/nlohmann/fifo_map)
Makes use of inih by benhoyt, licensed under BSD (https://github.com/benhoyt/inih)
